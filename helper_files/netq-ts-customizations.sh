#!/bin/bash
#
# You might think you don't need these customizations, but....ya do.
# There needs to be at least a no-op here. The CI pipeline checks for the air-agent successfully
# executing *something* and then deleting those instructions. The CI Pipeline will progress when no instructions are detected
#
echo "hey ya'll this is where customizations go"
