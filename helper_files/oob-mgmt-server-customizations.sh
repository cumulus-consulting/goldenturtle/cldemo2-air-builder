#!/bin/bash
#
# This runs as root. Users use the system as cumulus
# mind your chmods and chowns for things you want user cumulus to use
#
# clone test drive automation repo for on demand test drives
git clone https://github.com/CumulusNetworks/cumulus-test-drive /home/cumulus/Test-Drive-Automation
chown -R cumulus:cumulus /home/cumulus/Test-Drive-Automation

# add some ansible ssh convenience settings so ad hoc ansible works easily
cat <<EOT >> /etc/ansible/ansible.cfg
[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null
EOT
chmod -R 755 /etc/ansible/*

apt update -qy
apt install -qy ntp
